#include "attenuated_output.hpp"
#include <city/O1.hpp>
#include <city/OLogN.hpp>
#include <city/random_selector.hpp>

#include <chrono>
#include <fstream>
#include <iostream>

template<class Testee>
void add_statics( Testee& testee )
{
    //These are generally using a previous year's population, to help test the updating

    testee.upsert("Wuhan",8364977);
    testee.upsert("Xi-an Shaanxi",8000965);
    testee.upsert("Ahmedabad",8059441);
    testee.upsert("New York City",8283550);
    testee.upsert("Kuala Lumpur",7996830);
    testee.upsert("Hangzhou",7642147);
    testee.upsert("Hong Kong",7547652);
    testee.upsert("Surat",7184590);
    testee.upsert("Dongguan",7407852);
    testee.upsert("Suzhou",7069992);
    testee.upsert("Foshan",7326852);
    testee.upsert("Riyadh",7231447);
    testee.upsert("Shenyang",7220104);
    testee.upsert("Baghdad",7144260);
    testee.upsert("Dar es Salaam",6701650);
    testee.upsert("Santiago",6767223);
    testee.upsert("Pune",6629347);
    testee.upsert("Madrid",6617513);
    testee.upsert("Haerbin",6387195);

    testee.upsert( "Ibadan", 3649023 );
    testee.upsert( "Malappuram", 3608928 );
    testee.upsert( "Berlin", 3566791 );
    testee.upsert( "Tangshan Hebei", 3562719 );
    testee.upsert( "Faisalabad", 3542020 );
    testee.upsert( "Antananarivo", 3531887 );
    testee.upsert( "Bekasi", 3510050 );
    testee.upsert( "Kumasi", 3490030 );
    testee.upsert( "Kampala", 3469510 );
    testee.upsert( "Busan", 3465946 );
    testee.upsert( "Abuja", 3464123 );
    testee.upsert( "Guiyang", 3407463 );
    testee.upsert( "Asuncion", 3394309 );
    testee.upsert( "Santo Domingo", 3388809 );
    testee.upsert( "Campinas", 3343816 );
    testee.upsert( "Wuxi", 3315113 );
    testee.upsert( "Mashhad", 3264101 );
    testee.upsert( "Puebla", 3244710 );
    testee.upsert( "Dakar", 3229800 );

    testee.upsert( "Jeonju", 663607 );
    testee.upsert( "Bijie", 662697 );
    testee.upsert( "Portland", 662549 );
    testee.upsert( "Sokoto", 662173 );
    testee.upsert( "Qinzhou", 661953 );
    testee.upsert( "Macao", 661838 );
    testee.upsert( "Samsun", 660863 );
    testee.upsert( "Denizli", 660643 );
    testee.upsert( "Zhoushan", 659777 );
    testee.upsert( "Busto Arsizio", 657698 );
    testee.upsert( "Tongxiang", 656136 );
    testee.upsert( "Longhai", 655081 );
    testee.upsert( "Puyang", 654261 );
    testee.upsert( "Hezhou", 654185 );
    testee.upsert( "Jhansi", 653879 );
    testee.upsert( "Neijiang", 653866 );
    testee.upsert( "Izhevsk", 651601 );
    testee.upsert( "Memphis", 651011 );
    testee.upsert( "Jamnagar", 650977 );
    
    testee.upsert("Beirut" ,2424425);
    testee.upsert("Tunis",2365201);
    testee.upsert("Tangerang",2338547);
    testee.upsert("Mogadishu",2282009);
    testee.upsert("Baku",2341443);
    testee.upsert("Medan",2337958);
    testee.upsert("Belem",2334462);
    testee.upsert("Nantong",2275777);
    testee.upsert("Sendai",2327417);
    testee.upsert("Houston",2321960);
    testee.upsert("Manaus",2260788);
    testee.upsert("Barranquilla",2272914);
    testee.upsert("Maracaibo",2257999);
    testee.upsert("Gujranwala",2229220);
    testee.upsert("Rawalpindi",2236905);
    testee.upsert("Peshawar",2202946);
    testee.upsert("Taoyuan",2245162);
    testee.upsert("Agra",2210246);
    testee.upsert("Hohhot",2163394);

    testee.upsert( "Guang-an", 503551 );
    testee.upsert( "Tirana", 502734 );
    testee.upsert( "Surat Thani", 502564 );
    testee.upsert( "Kansas City", 501957 );
    testee.upsert( "Ubon Ratchathani", 501747 );
    testee.upsert( "Kolwezi", 501375 );
    testee.upsert( "Kabinda", 501054 );
    testee.upsert( "Jianyang", 500925 );
    testee.upsert( "Douai-Lens", 500921 );
    testee.upsert( "Nanping", 500785 );
}

template<class Impl>
void run( unsigned long seed )
{
    city::random_selector<Impl> testee;
    testee.seed( seed );
    std::ifstream city_file{ "cities.csv" };//https://worldpopulationreview.com/world-cities
    std::string discard, name;
    char comma, quote;
    long population;
    std::getline( city_file, discard );//discard the header
    auto start = std::chrono::high_resolution_clock::now();
    add_statics( testee );
    for ( auto count = 0UL; city_file && count < seed; ++count )
    {
        std::getline( city_file, discard, ',' );//Discard rank
        city_file >> quote;
        assert( quote == '"' );
        std::getline( city_file, name, '"' );
        city_file >> comma; 
        if ( comma != ',' ) {
            break;
        }
        std::getline( city_file, discard, ',' );//Discard country
        city_file >> quote;
        assert( quote == '"' );
        population = -1;
        city_file >> population;
        std::getline( city_file, discard, '\n' );//Discard remainder of row
        testee.upsert( name, population );        
    }
    auto elapsed = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Adding " << testee.underlying().urbanite_count()
        << " people in " << testee.underlying().city_count()
        << " cities and setting up the data structure took: "
        << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count()
        << " ms.\n";
    attenuated_output out{ std::to_string(seed) + "." + Impl::name() + ".txt" };
    start = std::chrono::high_resolution_clock::now();
    for ( auto count = 0UL; count < seed * seed; ++count )
    {
        auto answer = testee.get_city_of_randomly_selected_urbanite();
        out.write( answer );
    }
    elapsed = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Finding " << (seed*seed) << " random people and reporting their cities took: "
        << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count()
        << " ms.\n";
}

int main( int argc, char const* argv[] )
{
    if ( argc < 3 ) {
        std::clog << "Specify the O() as an argument, either: 1 or ln N , followed by a seed/lines of output.\n";
        return EXIT_FAILURE;
    }
    if ( std::toupper(*(argv[1])) == 'L' )
    {
        run<city::OLogN_selector>( std::stoul(argv[2]) );
    }
    else
    {
        run<city::O1_selector>( std::stoul(argv[2]) );
    }
}

