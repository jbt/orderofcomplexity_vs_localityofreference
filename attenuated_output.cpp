#include "attenuated_output.hpp"

void attenuated_output::write( std::string_view answer )
{
    if ( ++line_ % freq_ ) {
        return;
    }
    file_ << answer << '\n';
    if ( at_freq_++ > freq_ )
    {
        at_freq_ = 0;
        ++freq_;
    }
}
