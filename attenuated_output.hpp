#pragma once

#include <fstream>
#include <string>

class attenuated_output
{
public:
    explicit attenuated_output( std::string const& file_name )
    : file_{file_name}
    {}
    
    void write( std::string_view );
    
private:
    std::ofstream file_;
    std::size_t line_ = 0;
    std::size_t freq_ = 1;
    std::size_t at_freq_ = 0;
};
