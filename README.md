# OrderOfComplexity_vs_LocalityOfReference

I was curious if my intuition about these two solutions to the same problem was right - that the O(log N) solution would actually be faster than the naive O(1) solution.

The question is, my own words: given a list of all the cities in the world and their populations, randomly select a city dweller (with equal weight) and report the city they live in.

The O(log N) solution uses a binary search across a vector of the cities (and some metadata in each element). 
The O(1) solution has a vector with one element for each urbanite, that references the name of their city, and simply indexes that.

To be clear, we're ignoring setup time, all the modifications of the data structure. 
Yes, of course the O(1) version always takes much longer to set up. In these test cases it ranges from 2662ms to 146286ms, where the O(log N) version ranges from 0ms to 5ms. Allocations matter.
But that's not the point. The original question suggests the read operation will happen 'infinite' times. So however long it takes it dominates.

<style type="text/css" rel="stylesheet">th, td {border: 1px solid black;}</style>

milliseconds are rounded down and refer to the read operations

| Cities | People     | N Output    | ms O(log N) | ms O(1) |
|--------|------------|-------------|-------------|---------|
| 87     | 303313720  | 1           | 0           | 0       |
| 96     | 510186138  | 100         | 0           | 0       |
| 167    | 1084604168 | 10000       | 1           | 0       |
| 1005   | 2110322641 | 1000000     | 75          | 55      |
| 1163   | 6487188552 | 100000000   | 7619        | 9715    |
| 1163   | 6487188552 | 10000000000 | 639909      | 983920  |

## Environment 

The 'environment' is my laptop, a servw12. Not a VM. Also not a realtime kernel. 
Running the stock distro and the stock full graphical desktop environment, which is a significant source of error.

You'll notice I changed the structure of strings in the O(1) solution to avoid std::bad_alloc, so...
````
$ head /proc/meminfo 
MemTotal:       65818572 kB
MemFree:        49089180 kB
MemAvailable:   58533372 kB
````

Since run.sh specifies -march=native, you may want to know:
````
$ head /proc/cpuinfo 
processor	: 0
vendor_id	: AuthenticAMD
cpu family	: 23
model		: 113
model name	: AMD Ryzen 9 3900 12-Core Processor
stepping	: 0
microcode	: 0x8701013
cpu MHz		: 2196.168
cache size	: 512 KB
physical id	: 0
````
