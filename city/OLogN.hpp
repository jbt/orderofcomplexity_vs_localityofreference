#pragma once

#include "citydefs.hpp"

namespace city {
    
    class OLogN_selector
    {
    public:
        static std::string name() { return "Logarithmic"; }

        void upsert( std::string const& city_name, Pop current_population );
        std::size_t city_count() const { return cities_.size(); }
        std::string_view get_city_of_person( Id id ) const;
        Id urbanite_count() const;
        void dump_cities( std::ostream& ) const;

    private:
        struct City
        {
            std::string name_;
            Pop pop_;
            Id  run_;//accumulation of pop_ across *this and cities with names lexicographically less
        };
        std::vector<City> cities_;
        
        bool internally_consistent() const;
    }; 

}
