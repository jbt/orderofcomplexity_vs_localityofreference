#include "OLogN.hpp"
#include <cassert>

using LogN = city::OLogN_selector;

void LogN::upsert( std::string const& city_name, Pop current_population )
{
    auto it = std::lower_bound( cities_.begin(), cities_.end(), city_name, [](City const&c,std::string const&n){return c.name_<n;} );
    if ( it == cities_.end() || it->name_ != city_name )
    {
        it = cities_.insert( it, City{city_name, current_population, current_population} );
    }
    else
    {
        it->pop_ = current_population;
    }
    auto running = it == cities_.begin() ? 0 : std::prev(it)->run_;
    for ( ; it != cities_.end(); ++it ) 
    {
        it->run_ = ( running += it->pop_ );
    }
    assert( internally_consistent() );
}

auto LogN::urbanite_count() const -> Id
{
    assert( cities_.size() > 0 );
    auto& last_city = *std::prev(cities_.end());
    assert( last_city.name_.size() > 0 );
    return last_city.run_;
}

auto LogN::get_city_of_person( Id id ) const -> std::string_view
{
    assert( internally_consistent() );
    #ifndef NDEBUG
        auto by_run = [](City const&c,City const&d){return c.run_<d.run_;};
    #endif
    assert( std::is_sorted(cities_.begin(), cities_.end(), by_run) );
    City dummy{ "", 0, id };
    auto it = std::upper_bound( cities_.begin(), cities_.end(), id, [](Id i,City const&c){return i<c.run_;} );
    assert( it >= cities_.begin() );
    assert( it < cities_.end() );
    return it->name_;
}

void LogN::dump_cities( std::ostream& str ) const
{
    for ( auto& c : cities_ )
    {
        str << c.name_ << ", pop: " << c.pop_ << '\n';
    }
}

auto LogN::internally_consistent() const -> bool
{
    City const* prev = nullptr;
    for ( auto& c : cities_ )
    {
        assert(c.pop_ > 0);
        if ( c.pop_ < 1 ) {
            return false;
        }
        assert(c.name_.size()>0U);
        if ( c.name_.empty() ) {
            return false;
        }
        if ( prev )
        {
            assert(c.name_ > prev->name_);
            if ( c.name_ <= prev->name_ ) {
                return false;
            }
            assert(c.run_ == prev->run_ + c.pop_);
            if ( c.run_ != prev->run_ + c.pop_ ) {
                return false;
            }
        }
        else if ( c.run_ != c.pop_ )
        {
            assert(c.run_ == c.pop_);
            return false;
        }
        prev = &c;
    }
    return true;
}
