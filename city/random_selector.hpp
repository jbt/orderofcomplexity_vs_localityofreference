#pragma once

#include "citydefs.hpp"
#include <cassert>
#include <random>

namespace city {
    
    template<class Impl>
    class random_selector
    {
    public:

        void upsert( std::string const& city_name, Pop current_population ) {
            impl_.upsert( city_name, current_population );
        }
//         std::pair<Id,std::string_view> get_city_of_randomly_selected_urbanite()
        std::string_view get_city_of_randomly_selected_urbanite()
        {
            auto upper_bound = impl_.urbanite_count();
            assert( upper_bound > 0 );
            auto selected_id = rnd( upper_bound );
            assert( selected_id < upper_bound );
//             return { selected_id, impl_.get_city_of_person(selected_id) };
            return impl_.get_city_of_person(selected_id);
        }
        void seed( Id seed )
        {
            re_ = std::default_random_engine{seed};
        }
        
        Impl const& underlying() const {
            return impl_;
        }
        
    private:
        Id rnd( Id upper )
        {
            assert( upper > 0 );
            if ( upper > dist_.max() + 1 ) {
                dist_ = decltype(dist_){ 0, upper + 1 };
            }
            return dist_( re_ ) % upper;
        }
        
        Impl impl_;
        std::default_random_engine re_;
        std::uniform_int_distribution<Id> dist_;
    };

}
