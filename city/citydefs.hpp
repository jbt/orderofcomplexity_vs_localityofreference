#pragma once

#include <cstdint>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

namespace city {

    using Pop = std::uint_least32_t;//Large enough for the population of any single city
    using Id  = std::uint_least64_t;//Large enough for the population of all cities, can enumerate individual people

}
