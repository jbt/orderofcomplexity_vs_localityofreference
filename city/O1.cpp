#include "O1.hpp"
#include <cassert>

using O1 = city::O1_selector;

void O1::upsert( std::string const& city_name, Pop current_population )
{
    ces cn{ city_name };
    auto lookup_range = std::equal_range( lookup_.begin(), lookup_.end(), cn );
    auto old_pop = std::distance( lookup_range.first, lookup_range.second );
    if ( old_pop > current_population )
    {
//         std::cout << "Population of " << city_name << " decreased from " << old_pop << " to " << current_population << '\n';
        lookup_.erase( lookup_range.first, std::next(lookup_range.first, old_pop - current_population) );
    }
    else if ( old_pop < current_population )
    {
        if ( !old_pop ) {
            ++city_count_;
        }
        lookup_.insert( lookup_range.second, current_population - old_pop, cn );
    }
    assert( internally_consistent() );
}

auto O1::get_city_of_person( Id id ) const -> std::string_view
{
    return lookup_.at(id);
}
void O1::dump_cities( std::ostream& str ) const
{
    for ( auto it = lookup_.begin(); it != lookup_.end(); )
    {
        auto e = std::find_if( it, lookup_.end(), [&it](auto s){return*it!=s;} );
        str << it->to_sv() << ", pop: " << std::distance(it, e) << '\n';
        it = e;
    }
}

auto O1::internally_consistent() const -> bool
{
    return true;//This is far too slow to run unless we have reason to believe there is a problem
    /*
    auto it = std::adjacent_find( lookup_.begin(), lookup_.end(), [](auto a,auto b){return a>b;} );
    if ( it == lookup_.end() ) {
        return true;
    }
    auto other = std::next(it);
    assert( *it == *other );
    return *it == *other;
    */
}
