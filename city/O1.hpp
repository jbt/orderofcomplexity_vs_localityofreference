#pragma once

#include "citydefs.hpp"
#include <compact_externalized_strings.hpp>
#include <list>

namespace city {

    class O1_selector 
    {
    public:
        static std::string name() { return "Constant"; }
        
        void upsert( std::string const& city_name, Pop current_population );
        std::size_t city_count() const { return city_count_; }
        Id urbanite_count() const { return lookup_.size(); }
        std::string_view get_city_of_person( Id id ) const;
        void dump_cities( std::ostream& ) const;

    private:
        using NameIdx = std::uint_least16_t;
        std::vector<ces> lookup_;
        short city_count_ = 0;
        
        bool internally_consistent() const;        
    };

}
