#include "compact_externalized_strings.hpp"
#include <cassert>
#include <string>
#include <vector>

namespace {
    std::vector<std::string> storage;
}

ces::ces( std::string_view sv )
{
    auto it = std::find( storage.begin(), storage.end(), sv );
    using off_t = decltype(off);
    if ( it == storage.end() )
    {
        assert( storage.size() <= std::numeric_limits<off_t>::max() );
        off = static_cast<off_t>( storage.size() );
        storage.emplace_back( sv );
    }
    else
    {
        off = static_cast<off_t>( std::distance(storage.begin(), it) );
    }
}

std::string_view ces::to_sv() const
{
    return storage.at( off );
}
