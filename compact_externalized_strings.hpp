#include <cstdint>
// #include <iomanip>
#include <string_view>

class ces
{
    std::uint_least16_t off;
public:
    ces( std::string_view );
    
    std::string_view to_sv() const;
    operator std::string_view() const { return to_sv(); }
    bool operator<( ces rhs ) const {
        return to_sv() < rhs.to_sv();
    }
    bool operator==( ces rhs ) const {
        return off == rhs.off;
    }
};

// std::ostream& operator<<( std::ostream&, ces );
