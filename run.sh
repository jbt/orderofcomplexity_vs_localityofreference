#!/bin/bash -xe

rm -rv build || echo Welcome.
build() {
    b="build/${1}"
    mkdir -p "${b}"
    shift
    c++ -std=c++20 `find -name '*.cpp'` -o "${b}/rpc" -I . -W{all,extra,pedantic,error} -fmax-errors=2 "${@}" 
}
run() {
    mkdir -p build/${1}
    cp -v cities.csv build/${1}
    (
        cd build/${1}
        shift
        for calls in ${@}
        do
            for algo in Logarithmic Constant 
            do
                pwd
                time ./rpc ${algo} ${calls} 2>&1 | tee ${calls}.${algo}.out
            done
            wc ${calls}.*.txt
            pwd
            diff ${calls}.*.txt
        done
    )
}

build san -O0 -g{,gdb}3 -fsanitize=address -D_GLIBCXX_DEBUG=1
build dbg -O0 -g{,gdb}3 
build opt -Ofast -g0 -DNDEBUG=1 -flto -march=native -mtune=native
strip build/opt/rpc
# run san 1 
run dbg 1 10
run opt 1 10 100 1000 10000 100000
